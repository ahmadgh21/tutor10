import sys
from selenium import webdriver
#from selenium.webdriver.common.keys import Keys
#import unittest
#from django.conf import settings
#settings.configure()
#from lists import models
#from django.test import LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
#from unittest import skip


class FunctionalTest(StaticLiveServerTestCase):
	@classmethod
	def setUpClass(cls):  #1
		for arg in sys.argv:  #2
			if 'liveserver' in arg:  #3
				cls.server_url = 'http://' + arg.split('=')[1]
				return  #5
		super().setUpClass()  #6
		cls.server_url = cls.live_server_url

	@classmethod
	def tearDownClass(cls):
		if cls.server_url == cls.live_server_url:
			super().tearDownClass()

	def setUp(self):  #2
		self.browser = webdriver.Firefox()

	def tearDown(self):  #3
		self.browser.quit()

	def check_for_row_in_list_table(self, row_text):
		table = self.browser.find_element_by_id('id_list_table')
		rows = table.find_elements_by_tag_name('tr')
		self.assertIn(row_text, [row.text for row in rows])



