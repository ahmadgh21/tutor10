from .base import FunctionalTest
#import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
#import unittest
#from django.conf import settings
#settings.configure()
#from lists import models
#from django.test import LiveServerTestCase
#from django.contrib.staticfiles.testing import StaticLiveServerTestCase
#from unittest import skip



class NewVisitorTest(FunctionalTest):  

	def test_can_start_a_list_and_retrieve_it_later(self):  #4
		# Edith has heard about a cool new online to-do app. She goes
		# to check out its homepage
		#self.browser.get('http://localhost:8504')
		self.browser.get(self.server_url)
		# She notices the page title and header mention to-do lists
		self.assertIn('Personal HomePage', self.browser.title)
		header_text = self.browser.find_element_by_tag_name('h1').text
		self.assertIn('Start a new To-Do list', header_text)
		# She is invited to enter a to-do item straight away
		inputbox = self.browser.find_element_by_id('id_new_item')
		self.assertEqual(
			inputbox.get_attribute('placeholder'),
			'Enter a to-do item'
		)
	
		# She types "Buy peacock feathers" into a text box (Edith's hobby
		# is tying fly-fishing lures)
		inputbox.send_keys('Buy peacock feathers')

		# When she hits enter, the page updates, and now the page lists
		# "1: Buy peacock feathers" as an item in a to-do list table
		inputbox.send_keys(Keys.ENTER)
		edith_list_url = self.browser.current_url
		self.assertRegex(edith_list_url, '/lists/.+') #1
		self.check_for_row_in_list_table('1: Buy peacock feathers')	

		inputbox = self.browser.find_element_by_id('id_new_item')
		inputbox.send_keys('Use peacock feathers to make a fly')
		inputbox.send_keys(Keys.ENTER)
		# The page updates again, and now shows both items on her list
		self.check_for_row_in_list_table('1: Buy peacock feathers')
		self.check_for_row_in_list_table('2: Use peacock feathers to make a fly')
		
		self.browser.quit()
		self.browser = webdriver.Firefox()

		# Francis visits the home page.  There is no sign of Edith's
		# list
		self.browser.get(self.server_url)
		page_text = self.browser.find_element_by_tag_name('body').text
		self.assertNotIn('Buy peacock feathers', page_text)
		self.assertNotIn('make a fly', page_text)

		# Francis starts a new list by entering a new item. He
		# is less interesting than Edith...
		inputbox = self.browser.find_element_by_id('id_new_item')
		inputbox.send_keys('Buy milk')
		inputbox.send_keys(Keys.ENTER)

		# Francis gets his own unique URL
		francis_list_url = self.browser.current_url
		self.assertRegex(francis_list_url, '/lists/.+')
		self.assertNotEqual(francis_list_url, edith_list_url)
		
		# Again, there is no trace of Edith's list
		page_text = self.browser.find_element_by_tag_name('body').text
		self.assertNotIn('Buy peacock feathers', page_text)
		self.assertIn('Buy milk', page_text)


		#table = self.browser.find_element_by_id('id_list_table')
		#rows = table.find_elements_by_tag_name('tr')
		#daftar_komentar = ['yey, waktunya libur','sibuk tapi santai', 'oh tidak']
		#komentar = self.browser.find_element_by_tag_name('p')
		#self.assertIn(komentar.text,["komentar: "+kata for kata in daftar_komentar])
		#self.assertIn('1: Buy peacock feathers', [row.text for row in rows])
		
		
		#self.fail('Finish the test!')

		# She is invited to enter a to-do item straight away
